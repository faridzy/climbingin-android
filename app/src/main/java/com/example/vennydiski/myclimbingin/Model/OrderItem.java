package com.example.vennydiski.myclimbingin.Model;

/**
 * Created by mfarid on 04/06/18.
 */

public class OrderItem {

    int order_item_id;
    String climbing_tool_name,price,quantity,picture;

    public OrderItem(int order_item_id, String climbing_tool_name, String price, String quantity, String picture) {
        this.order_item_id = order_item_id;
        this.climbing_tool_name = climbing_tool_name;
        this.price = price;
        this.quantity = quantity;
        this.picture = picture;
    }


    public int getOrder_item_id() {
        return order_item_id;
    }

    public void setOrder_item_id(int order_item_id) {
        this.order_item_id = order_item_id;
    }

    public String getClimbing_tool_name() {
        return climbing_tool_name;
    }

    public void setClimbing_tool_name(String climbing_tool_name) {
        this.climbing_tool_name = climbing_tool_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
