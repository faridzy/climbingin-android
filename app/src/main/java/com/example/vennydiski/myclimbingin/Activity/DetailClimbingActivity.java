package com.example.vennydiski.myclimbingin.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.vennydiski.myclimbingin.Adapter.ClimbingCategoryAdapter;
import com.example.vennydiski.myclimbingin.Config.AppConfig;
import com.example.vennydiski.myclimbingin.Config.AppController;
import com.example.vennydiski.myclimbingin.Model.ClimbingTool;
import com.example.vennydiski.myclimbingin.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mfarid on 23/06/18.
 */

public class DetailClimbingActivity extends AppCompatActivity {
    public List<ClimbingTool> climbingList;
    public ClimbingCategoryAdapter adapterClimbingCategory;
    Context context;
    ImageView buttonCart;
    Button addToCart;
    int userId, climbingItemId;
    String currentYear;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_climbing);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.di_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView title = (TextView)findViewById(R.id.di_toolbar_title);
        title.setText(getIntent().getExtras().getString("climbing_tool_name"));

        buttonCart = (ImageView) findViewById(R.id.button_cart);
        buttonCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cart = new Intent(getApplicationContext(), CartActivity.class);
                startActivity(cart);
            }
        });

        String climbingName = getIntent().getExtras().getString("climbing_tool_name");
        String climbingQuantity = getIntent().getExtras().getString("climbing_tool_stock");
        String climbingPrice = getIntent().getExtras().getString("climbing_tool_price");
        String imgUrl = AppConfig.ROOT_URL+getIntent().getExtras().getString("climbing_tool_picture");
        Log.d("imgUrl", imgUrl);

        ImageView imageView = (ImageView)findViewById(R.id.di_climbing_picture);
        TextView detailClimbingName = (TextView)findViewById(R.id.di_climbing_name);
        TextView detailClimbingQuantity = (TextView)findViewById(R.id.di_climbing_quantity);
        TextView detailClimbingPrice = (TextView)findViewById(R.id.di_climbing_price);


        detailClimbingName.setText(climbingName);
        detailClimbingQuantity.setText(climbingQuantity);
        detailClimbingPrice.setText("Rp. "+AppConfig.formatDecimal(Integer.valueOf(climbingPrice)));
        Glide.with(getApplicationContext()).load(imgUrl)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(imageView);


        progressBar = (ProgressBar)findViewById(R.id.progress_bar_add_to_cart);
        addToCart = (Button) findViewById(R.id.button_add_to_cart);
        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                SharedPreferences preferences = getApplicationContext().getSharedPreferences("UserPref", 0);
                userId =  preferences.getInt("id", 0);
                climbingItemId = getIntent().getExtras().getInt("id");
                currentYear= Integer.toString(Calendar.getInstance().get(Calendar.YEAR));
                addToCart();
            }
        });

    }


    private void addToCart()
    {
        Log.d("url", AppConfig.URL_RECOMENDATION);
        final StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                AppConfig.URL_ADD_TO_CART,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("code") == 302) {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                                Intent cart = new Intent(getApplicationContext(), CartActivity.class);
                                startActivity(cart);
                            }
                            else {
                                //Log.d("masuk", "gagal");
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }

                    }
                },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d("error koneksi", error.getMessage());
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Gagal Koneksi", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", Integer.toString(userId));
                params.put("current_year", currentYear);
                params.put("order_climbing_tools_id", Integer.toString(climbingItemId));
                return params;
            }
        };
        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }



}
