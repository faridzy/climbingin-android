package com.example.vennydiski.myclimbingin.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.vennydiski.myclimbingin.Activity.EditProfileActivity;
import com.example.vennydiski.myclimbingin.Activity.LoginActivity;
import com.example.vennydiski.myclimbingin.Config.AppConfig;
import com.example.vennydiski.myclimbingin.Config.SessionManager;
import com.example.vennydiski.myclimbingin.R;

import java.util.HashMap;

/**
 * Created by mfarid on 23/06/18.
 */

public class ProfileFragment extends Fragment{
    TextView nameTV,usernameTV,emaiilTV,addressTV,identitynumberTV,postcodeTV,phonenumberTV,phonenumberAtasTV;
    Button mlogout;
    ImageView profilePicture;
    SessionManager session;
    Button editProfile;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        session = new SessionManager(getContext());
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        GetAllTextViewfromFragment_tab_profile(rootView);
        SetAllUserBiodataToTextView();
        phonenumberAtasTV.setText(String.valueOf(session.getIdUser()));
        mlogout = (Button) rootView.findViewById(R.id.but_profile_logout);
        mlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(session.logoutUser()){
                    Intent i = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
                    startActivity(i);
                    getActivity().finish();
                }
            }
        });
        editProfile = (Button)rootView.findViewById(R.id.button_edit_profile);
        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity().getApplicationContext(), EditProfileActivity.class);
                startActivity(intent);
            }
        });
        return rootView;
    }

    private void GetAllTextViewfromFragment_tab_profile(View rootView) {
        nameTV = (TextView) rootView.findViewById(R.id.tvProfile_NamaLengkap);
        usernameTV = (TextView) rootView.findViewById(R.id.tvProfile_Username);
        emaiilTV = (TextView) rootView.findViewById(R.id.tvProfile_email);
        addressTV = (TextView) rootView.findViewById(R.id.tvProfile_address);
        identitynumberTV = (TextView) rootView.findViewById(R.id.tvProfile_nik);
        postcodeTV = (TextView) rootView.findViewById(R.id.tvProfile_postcode);
        phonenumberTV = (TextView) rootView.findViewById(R.id.tvProfile_phone_number);
        phonenumberAtasTV = (TextView) rootView.findViewById(R.id.tvprofile_NoHP_atas);
        profilePicture = (ImageView)rootView.findViewById(R.id.profile_picture);
    }

    private void SetAllUserBiodataToTextView() {
        HashMap<String, String> user = session.getUserDetails();
        nameTV.setText(user.get(SessionManager.KEY_USERNAME));
        emaiilTV.setText(user.get(SessionManager.KEY_EMAIL));
        usernameTV.setText(user.get(SessionManager.KEY_USERNAME));
        identitynumberTV.setText(user.get(SessionManager.KEY_IDENTITY_NUMBER));
        addressTV.setText(user.get(SessionManager.KEY_ADDRESS));
        postcodeTV.setText(user.get(SessionManager.KEY_POSTCODE));
        phonenumberTV.setText(user.get(SessionManager.KEY_PHONE_NUUMBER));

        String imgUrl = user.get(SessionManager.KEY_PICTURE);
        Log.d("picture", AppConfig.ROOT_URL+imgUrl);
        Glide.with(getActivity().getApplicationContext()).load(AppConfig.ROOT_URL + imgUrl)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(profilePicture);

    }
}
