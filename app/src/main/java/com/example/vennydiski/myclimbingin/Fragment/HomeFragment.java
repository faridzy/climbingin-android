package com.example.vennydiski.myclimbingin.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.vennydiski.myclimbingin.Activity.ClimbingCategoryActivity;
import com.example.vennydiski.myclimbingin.Activity.SearchActivity;
import com.example.vennydiski.myclimbingin.Adapter.HomeAdapter;
import com.example.vennydiski.myclimbingin.Config.AppConfig;
import com.example.vennydiski.myclimbingin.Config.AppController;
import com.example.vennydiski.myclimbingin.Model.ClimbingTool;
import com.example.vennydiski.myclimbingin.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ss.com.bannerslider.banners.Banner;
import ss.com.bannerslider.banners.RemoteBanner;
import ss.com.bannerslider.views.BannerSlider;

/**
 * Created by mfarid on 23/06/18.
 */

public class HomeFragment extends Fragment {

    CardView laut, tawar, udang;
    public List<ClimbingTool> recomendationList;
    public HomeAdapter adapter;
    Context context;
    TextView search;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        context = this.getActivity().getApplicationContext();
        //Slider
        BannerSlider bannerSlider = (BannerSlider) rootView.findViewById(R.id.banner_slider1);
        List<Banner> banners=new ArrayList<>();
        banners.add(new RemoteBanner(AppConfig.ROOT_URL+"public/uploads/banner/banner1.jpeg"));
        banners.add(new RemoteBanner(AppConfig.ROOT_URL+"public/uploads/banner/banner2.jpg"));
        bannerSlider.setBanners(banners);

        RecyclerView rv = (RecyclerView)(rootView.findViewById(R.id.rv_home));
        rv.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        rv.setLayoutManager(linearLayoutManager);
        recomendationList = new ArrayList<>();
        adapter = new HomeAdapter(recomendationList, context);
        rv.setAdapter(adapter);
        getRecomendation();

        laut = (CardView)(rootView.findViewById(R.id.category_one));
        udang = (CardView)(rootView.findViewById(R.id.category_two));
        tawar = (CardView)(rootView.findViewById(R.id.category_three));

        laut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("id", 2);
                bundle.putString("climbing_tool_category_name", "Fashions");
                Intent i = new Intent(getActivity(), ClimbingCategoryActivity.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        });


        udang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("id", 3);
                bundle.putString("climbing_tool_category_name", "Accessories");
                Intent i = new Intent(getActivity(), ClimbingCategoryActivity.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        tawar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("id", 4);
                bundle.putString("climbing_tool_category_name", "Climbing Items");
                Intent i = new Intent(getActivity(), ClimbingCategoryActivity.class);
                i.putExtras(bundle);
                startActivity(i);
            }
        });

        adapter.notifyDataSetChanged();


        search = (TextView)(rootView.findViewById(R.id.edttext_search_main_home));
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity().getApplicationContext(), SearchActivity.class);
                startActivity(intent);
            }
        });

        return rootView;

    }


    private void getRecomendation()
    {
        final StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                AppConfig.URL_RECOMENDATION,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("code") == 302) {
                                JSONArray jsonArrayFish = obj.getJSONArray("data");
                                refreshList(jsonArrayFish);
                            }
                            else {
                                Toast.makeText(getActivity().getApplicationContext(), "message", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }

                    }
                },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity().getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            //TODO NAMPILKAN HASMAP DARI VARIABBLE NULL DARI GET DATA WEB
        };
        AppController.getInstance(getActivity().getApplicationContext()).addToRequestQueue(stringRequest);

    }

    private void refreshList(JSONArray recomendations) throws JSONException {
        recomendationList.clear();
        for (int i = 0; i < recomendations.length(); i++) {
            JSONObject obj = recomendations.getJSONObject(i);
            Log.d("JSON", "valuenya :"+obj);
            recomendationList.add(new ClimbingTool(
                    obj.getInt("id"),
                    obj.getString("climbing_tool_name"),
                    obj.getString("climbing_tool_category_name"),
                    obj.getString("climbing_tool_price"),
                    obj.getString("climbing_tool_stock"),
                    obj.getString("climbing_tool_picture"),
                    obj.getString("climbing_tool_categories_id")
            ));
        }

        adapter.notifyDataSetChanged();
    }
}
