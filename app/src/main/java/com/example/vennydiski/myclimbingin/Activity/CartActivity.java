package com.example.vennydiski.myclimbingin.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.vennydiski.myclimbingin.Adapter.CartAdapter;
import com.example.vennydiski.myclimbingin.Config.AppConfig;
import com.example.vennydiski.myclimbingin.Config.AppController;
import com.example.vennydiski.myclimbingin.Model.OrderItem;
import com.example.vennydiski.myclimbingin.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mfarid on 23/06/18.
 */

public class CartActivity extends AppCompatActivity {
    public List<OrderItem> cartList;
    public CartAdapter adapterCart;
    Context context;
    ImageView buttonCart;
    int total = 0;
    TextView valueTotal;
    Button checkout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.cart_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        valueTotal = (TextView)findViewById(R.id.value_total_bayar);

        buttonCart = (ImageView) findViewById(R.id.button_cart);
        buttonCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cart = new Intent(getApplicationContext(), CartActivity.class);
                startActivity(cart);
            }
        });

        checkout = (Button)findViewById(R.id.cart_button_checkout);
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cart = new Intent(getApplicationContext(), CheckoutActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("total", Integer.toString(total));
                cart.putExtras(bundle);
                startActivity(cart);
                finish();
            }
        });

        SharedPreferences preferences = getApplicationContext().getSharedPreferences("UserPref", 0);
        int id = preferences.getInt("id", 0);

        RecyclerView rv = (RecyclerView)(findViewById(R.id.rv_cart));
        rv.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rv.setLayoutManager(linearLayoutManager);
        cartList = new ArrayList<>();
        adapterCart = new CartAdapter(cartList, context);
        rv.setAdapter(adapterCart);
        getCart(id);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void getCart(int userId)
    {
        final StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                AppConfig.URL_LIST_CART+userId+"/list",
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("code") == 302) {
                                JSONArray jsonListCart = obj.getJSONArray("data");
                                refreshData(jsonListCart);
                            }
                            else {
                                Toast.makeText(getApplicationContext(), "message", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }

                    }
                },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            //TODO NAMPILKAN HASMAP DARI VARIABBLE NULL DARI GET DATA WEB
        };
        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }


    private void refreshData(JSONArray recomendations) throws JSONException {
        cartList.clear();
        for (int i = 0; i < recomendations.length(); i++) {
            JSONObject obj = recomendations.getJSONObject(i);
            Log.d("JSON", "valuenya :"+obj);
            cartList.add(new OrderItem(
                    obj.getInt("order_item_id"),
                    obj.getString("climbing_tool_name"),
                    obj.getString("price"),
                    obj.getString("quantity"),
                    obj.getString("picture")
            ));

            total += (Integer.valueOf(obj.getString("price")) * Integer.valueOf(obj.getString("quantity")));
        }
        valueTotal.setText("Rp. "+AppConfig.formatDecimal(total));
        adapterCart.notifyDataSetChanged();
    }
}
