package com.example.vennydiski.myclimbingin.Config;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by Venny Diski on 03/06/2018.
 */
//buat menyimpan data untuk login
public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "UserPref";
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String IS_CART = "IsCart";
    private static final String KEY_ID = "id";
    public static final String KEY_NAME = "fullname";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_PHONE_NUUMBER = "phone_number";
    public static final String KEY_IDENTITY_NUMBER = "identity_number";
    public static final String KEY_POSTCODE = "post_code";
    public static final String KEY_PICTURE = "picture";

    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(String name,
                                   String email,
                                   String username,
                                   String identity_number,
                                   String address,
                                   String postcode,
                                   String phone_number,
                                   int id,
                                   String picture
    ){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_IDENTITY_NUMBER, identity_number);
        editor.putString(KEY_ADDRESS, address);
        editor.putString(KEY_POSTCODE, postcode);
        editor.putString(KEY_PHONE_NUUMBER, phone_number);
        editor.putInt(KEY_ID,id);
        editor.putString(KEY_PICTURE, picture);
        editor.commit();
    }

    public void createCartIn(){
        editor.putBoolean(IS_CART, true);
    }

    public int getIdUser(){
        return pref.getInt(KEY_ID,0);
    }

    public Boolean checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            return false;
        }
        return true;
    }

    public Boolean checkCart(){
        if (!this.isCartIn()){
            return false;
        }
        return true;
    }

    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_USERNAME,pref.getString(KEY_USERNAME,null));
        user.put(KEY_IDENTITY_NUMBER,pref.getString(KEY_IDENTITY_NUMBER,null));
        user.put(KEY_ADDRESS,pref.getString(KEY_ADDRESS,null));
        user.put(KEY_POSTCODE,pref.getString(KEY_POSTCODE,null));
        user.put(KEY_PHONE_NUUMBER,pref.getString(KEY_PHONE_NUUMBER,null));
        user.put(KEY_PICTURE, pref.getString(KEY_PICTURE, null));

        return user;
    }

    public Boolean isCartIn()
    {
        return pref.getBoolean(IS_CART,false);
    }


    public boolean logoutUser(){
        editor.clear();
        editor.commit();

        if(isLoggedIn()){
            return false;
        }else{
            return true;
        }

    }

    public boolean isLoggedIn()
    {
        return pref.getBoolean(IS_LOGIN, false);
    }
}
