package com.example.vennydiski.myclimbingin.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.vennydiski.myclimbingin.Adapter.HistoryAdapter;
import com.example.vennydiski.myclimbingin.Config.AppConfig;
import com.example.vennydiski.myclimbingin.Config.AppController;
import com.example.vennydiski.myclimbingin.Model.History;
import com.example.vennydiski.myclimbingin.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mfarid on 23/06/18.
 */

public class HistoryFragment extends Fragment {
    public HistoryAdapter adapter;
    public List<History> listHistory;
    Context context;

    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);
        context = getActivity().getApplicationContext();
        SharedPreferences preferences = getActivity().getApplicationContext().getSharedPreferences("UserPref", 0);
        int id = preferences.getInt("id", 0);

        RecyclerView rv = (RecyclerView)(rootView.findViewById(R.id.rv_history));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rv.setLayoutManager(linearLayoutManager);
        listHistory = new ArrayList<>();
        adapter = new HistoryAdapter(listHistory, context);
        rv.setAdapter(adapter);
        readHistories(id);

        return rootView;
    }


    private void readHistories(int userId)
    {
        final StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                AppConfig.URL_HISTORY+"/"+userId+"/all",
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("code") == 302) {
                                JSONArray jsonArrayFish = obj.getJSONArray("data");
                                refreshData(jsonArrayFish);
                            }
                            else {
                                Toast.makeText(getActivity().getApplicationContext(), "message", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }

                    }
                },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity().getApplicationContext(), "Gagal Koneksi", Toast.LENGTH_SHORT).show();
            }
        }){
            //TODO NAMPILKAN HASMAP DARI VARIABBLE NULL DARI GET DATA WEB
        };
        AppController.getInstance(getActivity().getApplicationContext()).addToRequestQueue(stringRequest);
    }


    private void refreshData(JSONArray recomendations) throws JSONException {
        listHistory.clear();
        for (int i = 0; i < recomendations.length(); i++) {
            JSONObject obj = recomendations.getJSONObject(i);
            Log.d("JSON", "valuenya :"+obj);
            listHistory.add(new History(
                    obj.getString("order_id"),
                    obj.getString("user_id"),
                    obj.getString("order_date"),
                    obj.getString("order_total"),
                    obj.getString("order_status"),
                    obj.getString("order_address"),
                    obj.getInt("order_feedback_status"),
                    obj.getString("order_is_pay")
            ));

        }

        adapter.notifyDataSetChanged();
    }
}
