package com.example.vennydiski.myclimbingin.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.vennydiski.myclimbingin.Adapter.ClimbingCategoryAdapter;
import com.example.vennydiski.myclimbingin.Config.AppConfig;
import com.example.vennydiski.myclimbingin.Config.AppController;
import com.example.vennydiski.myclimbingin.Model.ClimbingTool;
import com.example.vennydiski.myclimbingin.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 12/16/2017.
 */

public class ClimbingCategoryActivity extends AppCompatActivity {

    public List<ClimbingTool> climbingList;
    public ClimbingCategoryAdapter adapterClimbingCategory;
    Context context;
    ImageView buttonCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_climbing_category);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        //myToolbar.setTitle(getIntent().getExtras().getString("category_name"));
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView title = (TextView)findViewById(R.id.toolbar_title);
        title.setText(getIntent().getExtras().getString("category_tool_name"));

        buttonCart = (ImageView) findViewById(R.id.button_cart);
        buttonCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cart = new Intent(getApplicationContext(), CartActivity.class);
                startActivity(cart);
            }
        });

        Integer id = getIntent().getExtras().getInt("id");
        //Toast.makeText(getApplicationContext(), Integer.toString(id), Toast.LENGTH_LONG).show();

        RecyclerView rv = (RecyclerView)(findViewById(R.id.rv_fish_category));
        rv.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rv.setLayoutManager(linearLayoutManager);
        climbingList = new ArrayList<>();
        adapterClimbingCategory = new ClimbingCategoryAdapter(climbingList, context);
        rv.setAdapter(adapterClimbingCategory);
        getFishCategories(id);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;

            case R.id.action_favorite:
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    protected void getFishCategories(int categoryId)
    {
        //Log.d("iki get category", Integer.toString(categoryId));
        final StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                AppConfig.URL_ClIMBING_TOOL_BY_CATEGORY+categoryId,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("code") == 302) {
                                JSONArray jsonArrayFish = obj.getJSONArray("data");
                                refreshData(jsonArrayFish);
                            }
                            else {
                                Toast.makeText(getApplicationContext(), "message", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }

                    }
                },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            //TODO NAMPILKAN HASMAP DARI VARIABBLE NULL DARI GET DATA WEB
        };
        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void refreshData(JSONArray recomendations) throws JSONException {
        climbingList.clear();
        for (int i = 0; i < recomendations.length(); i++) {
            JSONObject obj = recomendations.getJSONObject(i);
            Log.d("JSON", "valuenya :"+obj);
            climbingList.add(new ClimbingTool(
                    obj.getInt("id"),
                    obj.getString("climbing_tool_name"),
                    obj.getString("climbing_tool_category_name"),
                    obj.getString("climbing_tool_price"),
                    obj.getString("climbing_tool_stock"),
                    obj.getString("climbing_tool_picture"),
                    obj.getString("climbing_tool_categories_id")
            ));
        }

        adapterClimbingCategory.notifyDataSetChanged();
    }


}