package com.example.vennydiski.myclimbingin.Model;

/**
 * Created by mfarid on 04/06/18.
 */

public class ClimbingTool {
    int id;
    String name,category_name,price,stock,picture,category_id;


    public ClimbingTool(int id, String name, String category_name, String price, String stock, String picture,String category_id) {
        this.id = id;
        this.name = name;
        this.category_name = category_name;
        this.price = price;
        this.stock = stock;
        this.picture = picture;
        this.category_id=category_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory_name() {
        return category_name;
    }

    public String getCategory_id() {
        return category_id;
    }


    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
