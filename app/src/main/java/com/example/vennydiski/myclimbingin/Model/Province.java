package com.example.vennydiski.myclimbingin.Model;

/**
 * Created by mfarid on 24/06/18.
 */

public class Province {
    String provinceId, provinceName;

    public Province(){};
    public Province(String provinceId, String provinceName)
    {
        this.provinceId = provinceId;
        this.provinceName = provinceName;
    }

    public void setProvinceId(String provinceId)
    {
        this.provinceId = provinceId;
    }

    public String getProvinceId()
    {
        return this.provinceId;
    }

    public void setProvinceName(String provinceName)
    {
        this.provinceName = provinceName;
    }

    public String getProvinceName()
    {
        return this.provinceName;
    }
}
