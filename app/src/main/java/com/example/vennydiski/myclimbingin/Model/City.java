package com.example.vennydiski.myclimbingin.Model;

/**
 * Created by mfarid on 24/06/18.
 */

public class City {

    String cityId, provinceId, provinceName, type, cityName, postalCode;

    public City(){}
    public City(String cityId, String provinceId, String provinceName, String type, String cityName, String postalCode){
        this.cityId = cityId;
        this.provinceId = provinceId;
        this.provinceName = provinceName;
        this.type = type;
        this.cityName = cityName;
        this.postalCode = postalCode;
    }

    public void setCityId(String cityId)
    {
        this.cityId = cityId;
    }

    public void setProvinceId(String provinceId)
    {
        this.provinceId = provinceId;
    }

    public void setProvinceName(String provinceName)
    {
        this.provinceName = provinceName;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public void setCityName(String cityName)
    {
        this.cityName = cityName;
    }

    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getCityId()
    {
        return this.cityId;
    }

    public String getProvinceId()
    {
        return this.provinceId;
    }

    public String getProvinceName()
    {
        return this.provinceName;
    }

    public String getType()
    {
        return this.type;
    }

    public String getCityName()
    {
        return this.cityName;
    }

    public String getPostalCode()
    {
        return this.postalCode;
    }
}
