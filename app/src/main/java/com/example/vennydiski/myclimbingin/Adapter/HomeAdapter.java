package com.example.vennydiski.myclimbingin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.vennydiski.myclimbingin.Activity.DetailClimbingActivity;
import com.example.vennydiski.myclimbingin.Config.AppConfig;
import com.example.vennydiski.myclimbingin.Model.ClimbingTool;
import com.example.vennydiski.myclimbingin.R;

import java.util.List;

/**
 * Created by mfarid on 23/06/18.
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeViewHolder> {

    List<ClimbingTool> recomendationList;
    private Context mContext;
    public HomeAdapter(List<ClimbingTool> recomendations, Context context){
        this.recomendationList = recomendations;
        this.mContext = context;
    }

    @Override
    public int getItemCount() {
        return recomendationList.size();
    }

    @Override
    public HomeViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        this.mContext = viewGroup.getContext();
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.home_view_holder, viewGroup, false);
        HomeViewHolder hvh = new HomeViewHolder(v);
        return hvh;
    }

    @Override
    public void onBindViewHolder(HomeViewHolder homeViewHolder, int i) {
        Log.d("Adapter", "item id : " + i);
        homeViewHolder.climbing = recomendationList.get(i);
        homeViewHolder.context = this.mContext;
        homeViewHolder.climbingName.setText(recomendationList.get(i).getName());
        //homeViewHolder.fishCategory.setText(recomendationList.get(i).getCategory_name());
        //homeViewHolder.singularPrice.setText(recomendationList.get(i).getSingular_price());
        Log.d("price", recomendationList.get(i).getPrice());
        homeViewHolder.price.setText("Rp. "+AppConfig.formatDecimal(Integer.valueOf(recomendationList.get(i).getPrice())));

        String imgUrl = recomendationList.get(i).getPicture();
        Glide.with(mContext).load(AppConfig.ROOT_URL + imgUrl)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(homeViewHolder.climbingPicture);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class HomeViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView climbingName;
        TextView climbingCategory;
        TextView price;
        ImageView climbingPicture;

        public ClimbingTool climbing;
        public Context context;

        HomeViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv_item_recommendation);
            climbingName = (TextView)itemView.findViewById(R.id.fish_name);
            //fishCategory = (TextView)itemView.findViewById(R.id.fish_category);
            //singularPrice = (TextView)itemView.findViewById(R.id.singular_price);
            price = (TextView)itemView.findViewById(R.id.collective_price);
            climbingPicture = (ImageView)itemView.findViewById(R.id.fish_picture);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", climbing.getId());
                    bundle.putString("climbing_tool_name", climbing.getName());
                    bundle.putString("climbing_tool_category_name", climbing.getCategory_name());
                    bundle.putString("climbing_tool_price", climbing.getPrice());
                    bundle.putString("climbing_tool_picture", climbing.getPicture());
                    bundle.putString("climbing_tool_category_id", climbing.getCategory_id());

                    Intent i = new Intent(((AppCompatActivity) context), DetailClimbingActivity.class);
                    i.putExtras(bundle);
                    context.startActivity(i);

                }
            });

        }
    }
}
