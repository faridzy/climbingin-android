package com.example.vennydiski.myclimbingin.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.example.vennydiski.myclimbingin.Adapter.MenuPagerAdapter;
import com.example.vennydiski.myclimbingin.Config.NonSwipeableViewPager;
import com.example.vennydiski.myclimbingin.Config.SessionManager;
import com.example.vennydiski.myclimbingin.R;


/**
 * Created by Venny Diski on 03/06/2018.
 */

public class MainActivity extends AppCompatActivity{
    SessionManager sessionManager;
    TabLayout mainTablayout;
    ViewPager mainTabPager ;
    Bundle categoryBundle;
    SessionManager session;
    private boolean enabled;
    ImageView buttonCart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonCart = (ImageView)findViewById(R.id.but_cart);
        buttonCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cart = new Intent(MainActivity.this, CartActivity.class);
                startActivity(cart);
            }
        });

        session = new SessionManager(getApplicationContext());
        mainTablayout = (TabLayout) findViewById(R.id.tablayout);
        final NonSwipeableViewPager mainTabPager = (NonSwipeableViewPager) findViewById(R.id.pager);
        final MenuPagerAdapter menuPagerAdapterTab = new MenuPagerAdapter( getSupportFragmentManager(), mainTablayout.getTabCount());
        mainTabPager.setMyScroller();
        mainTabPager.setAdapter(menuPagerAdapterTab);
        mainTabPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mainTablayout));

        mainTablayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mainTabPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                //TODO KOSONG
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                //TODO KOSONG
            }
        });


    }
}
