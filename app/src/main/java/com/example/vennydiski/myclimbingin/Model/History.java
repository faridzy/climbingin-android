package com.example.vennydiski.myclimbingin.Model;

/**
 * Created by mfarid on 24/06/18.
 */

public class History {
    public String orderId, userId,orderDate, orderTotal, orderStatus, orderAddress, orderName, orderIsPay;
    int orderFeedbackStatus;
    public History(String orderId, String userId,String orderDate, String orderTotal, String orderStatus, String orderAddress, int orderFeedbackStatus, String orderIsPay)
    {
        this.orderId = orderId;
        this.userId = userId;
        this.orderDate = orderDate;
        this.orderTotal = orderTotal;
        this.orderStatus = orderStatus;
        this.orderAddress = orderAddress;
        this.orderFeedbackStatus = orderFeedbackStatus;
        this.orderIsPay = orderIsPay;
    }

    public void setOrderIsPay(String orderIsPay)
    {
        this.orderIsPay = orderIsPay;
    }

    public String getOrderIsPay()
    {
        return this.orderIsPay;
    }

    public int getOrderFeedbackStatus()
    {
        return this.orderFeedbackStatus;
    }

    public String getOrderId()
    {
        return this.orderId;
    }

    public String getUserId()
    {
        return this.userId;
    }


    public String getOrderDate()
    {
        return this.orderDate;
    }

    public String getOrderTotal()
    {
        return this.orderTotal;
    }

    public String getOrderStatus()
    {
        return this.orderStatus;
    }

    public String getOrderAddress()
    {
        return this.orderAddress;
    }

    public String getOrderName()
    {
        return this.orderName;
    }
}
