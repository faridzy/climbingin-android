package com.example.vennydiski.myclimbingin.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vennydiski.myclimbingin.Activity.HelpFeedbackActivity;
import com.example.vennydiski.myclimbingin.Activity.HelpPembayaranActivity;
import com.example.vennydiski.myclimbingin.Activity.HelpPengirimanActivity;
import com.example.vennydiski.myclimbingin.Activity.JamOperasionalActivity;
import com.example.vennydiski.myclimbingin.R;

/**
 * Created by mfarid on 24/06/18.
 */

public class HelpFragment extends Fragment {
    public CardView jam, pembayaran, pengiriman, feedback;

    public static HelpFragment newInstance() {
        HelpFragment fragment = new HelpFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_help, container, false);

        jam = (CardView) (rootView.findViewById(R.id.cv_kategori));
        jam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cart = new Intent(getActivity().getApplicationContext(), JamOperasionalActivity.class);
                startActivity(cart);
            }
        });

        pembayaran = (CardView)(rootView.findViewById(R.id.cv_kategori2));
        pembayaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cart = new Intent(getActivity().getApplicationContext(), HelpPembayaranActivity.class);
                startActivity(cart);
            }
        });

        pengiriman = (CardView)(rootView.findViewById(R.id.cv_kategori3));
        pengiriman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cart = new Intent(getActivity().getApplicationContext(), HelpPengirimanActivity.class);
                startActivity(cart);
            }
        });

        feedback = (CardView)(rootView.findViewById(R.id.cv_kategori4));
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cart = new Intent(getActivity().getApplicationContext(), HelpFeedbackActivity.class);
                startActivity(cart);
            }
        });


        return rootView;
    }
}
