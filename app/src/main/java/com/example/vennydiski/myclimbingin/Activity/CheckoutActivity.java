package com.example.vennydiski.myclimbingin.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.vennydiski.myclimbingin.Config.AppConfig;
import com.example.vennydiski.myclimbingin.Config.AppController;
import com.example.vennydiski.myclimbingin.Model.City;
import com.example.vennydiski.myclimbingin.Model.Cost;
import com.example.vennydiski.myclimbingin.Model.Province;
import com.example.vennydiski.myclimbingin.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mfarid on 23/06/18.
 */

public class CheckoutActivity extends AppCompatActivity {
    public TextView subTotal, total, shippingCostLabel;
    public TextInputEditText receiverName, checkOutAddress,postalCode,phoneNumber;
    public Spinner spinner, provinceOption, cityOption, costOption;
    int paymentType;
    int shippingCostValue;
    Button process;
    ProgressBar progressBar;
    int userId;
    int weight;
    String userName;
    String userAddress;
    String userPhoneNumber;
    String userPostCode;
    public List<Province> provinceList;
    public List<City> cityList;
    public List<Cost> costList;
    String[] cityItems, provinceItems, costItems;
    public HashMap<Integer,String> provinceMap;
    public HashMap<Integer,String> cityMap;
    public HashMap<Integer,String> costMap;
    public HashMap<Integer, String> costValueMap;
    String sendingServiceCode;
    int globalTotal, globalSubTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        int shipping = 35000;
        try{
            cityList = new ArrayList<>();
            costList = new ArrayList<>();
            provinceList = new ArrayList<>();
            provinceMap = new HashMap<Integer, String>();
            cityMap = new HashMap<Integer, String>();
            costMap = new HashMap<Integer, String>();
            costValueMap = new HashMap<Integer, String>();

            getProvince();
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.checkout_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        receiverName = (TextInputEditText)findViewById(R.id.checkout_receiver_name);
        checkOutAddress = (TextInputEditText)findViewById(R.id.checkout_address);
        postalCode = (TextInputEditText)findViewById(R.id.checkout_postal_code);
        phoneNumber = (TextInputEditText)findViewById(R.id.checkout_phone_number);
        subTotal = (TextView)findViewById(R.id.checkout_total_order_items);
        total = (TextView)findViewById(R.id.textView3);
        shippingCostLabel = (TextView)findViewById(R.id.check_out_shipping_cost);


        String subTotalValue = getIntent().getExtras().getString("total");
        subTotal.setText("Rp. "+ AppConfig.formatDecimal(Integer.valueOf(subTotalValue)));
        globalSubTotal = Integer.valueOf(subTotalValue);

        //Inisialisasi nilai spinner
        spinner = (Spinner) findViewById(R.id.spinner_payment_option);
        String[] items = new String[]{"ClimbingPay", "Transfer ATM"};
        ArrayAdapter<String> adapterSpinner = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        spinner.setAdapter(adapterSpinner);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                paymentType = position + 1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        //Inisialisasi nilai option provinsi
        provinceOption = (Spinner)findViewById(R.id.spinner_province_option);
        cityOption = (Spinner)findViewById(R.id.spinner_city_option);
        costOption = (Spinner)findViewById(R.id.spinner_cost_option);

        //Proses pembayaran
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("UserPref", 0);
        userId = preferences.getInt("id", 0);
        userName = preferences.getString("name", "");
        userAddress = preferences.getString("address", "");
        userPhoneNumber = preferences.getString("phone_number", "");
        userPostCode = preferences.getString("postcode", "");

        receiverName.setText(userName);
        checkOutAddress.setText(userAddress);
        phoneNumber.setText(userPhoneNumber);
        postalCode.setText(userPostCode);


        progressBar = (ProgressBar) findViewById(R.id.progress_checkout);
        process = (Button)findViewById(R.id.checkout_button_process);
        process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                userName = receiverName.getText().toString().trim();
                userAddress = checkOutAddress.getText().toString().trim();
                userPhoneNumber = phoneNumber.getText().toString().trim();
                userPostCode = postalCode.getText().toString().trim();

                doCheckout();
            }
        });

    }

    private void doCheckout()
    {
        final StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                AppConfig.URL_CHECKOUT,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("code") == 302) {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                                try {
                                    Thread.sleep(200);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                                Intent cart = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(cart);
                            }
                            else {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }

                    }
                },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d("error koneksi", error.getMessage());
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Gagal Koneksi", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Log.d("paymentnya", Integer.toString(paymentType));

                Map<String, String> params = new HashMap<>();
                params.put("userid", Integer.toString(userId));
                params.put("destination_address", userAddress+" , "+userPostCode+" , "+userPhoneNumber);
                params.put("destination_name", userName);
                params.put("payment_type", Integer.toString(paymentType));
                params.put("service_code", sendingServiceCode);
                params.put("shipping", Integer.toString(shippingCostValue));
                Log.d("params", params.toString());
                return params;
            }
        };
        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    //Kebutuhan Spinner

    public void getProvince()
    {
        final StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                AppConfig.URL_GET_PROVINCE,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("code") == 302) {
                                JSONArray jsonArrayProvince = obj.getJSONArray("data");
                                refreshProvince(jsonArrayProvince);
                            }
                            else {
                                Toast.makeText(getApplicationContext(), "message", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }

                    }
                },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            //TODO NAMPILKAN HASMAP DARI VARIABBLE NULL DARI GET DATA WEB
        };
        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }


    public void getCity(int provinceId)
    {
        final StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                AppConfig.URL_GET_CITY+"?province="+provinceId,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("code") == 302) {
                                JSONArray jsonArrayCity = obj.getJSONArray("data");
                                refreshCity(jsonArrayCity);
                            }
                            else {
                                Toast.makeText(getApplicationContext(), "message", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }

                    }
                },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            //TODO NAMPILKAN HASMAP DARI VARIABBLE NULL DARI GET DATA WEB
        };
        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }


    public void refreshProvince(JSONArray provinces) throws JSONException
    {
        provinceList.clear();
        provinceItems = new String[provinces.length()];
        for (int i = 0; i < provinces.length(); i++) {
            JSONObject obj = provinces.getJSONObject(i);
            provinceList.add(new Province(
                    obj.getString("province_id"),
                    obj.getString("province")
            ));
            provinceMap.put(i, obj.getString("province_id") );
            provinceItems[i] = obj.getString("province");
        }

        ArrayAdapter<String> adapterProvince = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, provinceItems);
        provinceOption.setAdapter(adapterProvince);
        provinceOption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                //Toast.makeText(getApplicationContext(), Integer.toString(position), Toast.LENGTH_LONG).show();
                //panggil trigger update kota
                getCity(Integer.valueOf(provinceMap.get(position)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

    }


    public void refreshCity(JSONArray cities) throws JSONException
    {
        cityList.clear();
        cityItems = new String[cities.length()];
        for (int i = 0; i < cities.length(); i++) {
            JSONObject obj = cities.getJSONObject(i);
            cityList.add(new City(
                    obj.getString("city_id"),
                    obj.getString("province_id"),
                    obj.getString("province"),
                    obj.getString("type"),
                    obj.getString("city_name"),
                    obj.getString("postal_code")
            ));
            cityMap.put(i, obj.getString("city_id") );
            cityItems[i] = obj.getString("city_name");
        }

        ArrayAdapter<String> adapterCity = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, cityItems);
        cityOption.setAdapter(adapterCity);
        cityOption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                getCost(Integer.valueOf(cityMap.get(position)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

    }


    //Ambil biaya pengiriman
    private void getCost(final int destinationId)
    {
        final StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                AppConfig.URL_GET_COST,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("code") == 302) {
                                //progressBar.setVisibility(View.GONE);
                                JSONArray jsonArrayCost = obj.getJSONArray("data");
                                refreshCost(jsonArrayCost);

                            }
                            else {
                                //Log.d("masuk", "gagal");
                                //progressBar.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }

                    }
                },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d("error koneksi", error.getMessage());
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Gagal Koneksi", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("origin", Integer.toString(444));
                params.put("destination", Integer.toString(destinationId));
                params.put("weight", Integer.toString(5));

                return params;
            }
        };
        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }


    public void refreshCost(JSONArray costs) throws JSONException
    {
        costList.clear();
        costItems = new String[costs.length()];
        for (int i = 0; i < costs.length(); i++) {
            JSONObject obj = costs.getJSONObject(i);
            costList.add(new Cost(
                    obj.getString("service"),
                    obj.getString("description"),
                    obj.getString("cost"),
                    obj.getString("etd")
            ));
            costValueMap.put(i, obj.getString("cost"));
            costMap.put(i, obj.getString("service") );
            costItems[i] = obj.getString("service")+" ["+obj.getString("etd")+" days] - "+"Rp. "+AppConfig.formatDecimal(Integer.valueOf(obj.getString("cost")));
        }

        ArrayAdapter<String> adapterCost = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, costItems);
        costOption.setAdapter(adapterCost);
        costOption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                //Toast.makeText(getApplicationContext(), costMap.get(position), Toast.LENGTH_LONG).show();
                sendingServiceCode = costMap.get(position);
                shippingCostValue = Integer.valueOf(costValueMap.get(position));

                total.setText("Rp. "+AppConfig.formatDecimal( (Integer.valueOf(globalSubTotal) + shippingCostValue ) ));
                shippingCostLabel.setText("Rp. "+AppConfig.formatDecimal(shippingCostValue));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

    }
}
