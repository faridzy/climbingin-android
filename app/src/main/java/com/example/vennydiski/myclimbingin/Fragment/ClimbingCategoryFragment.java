package com.example.vennydiski.myclimbingin.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.vennydiski.myclimbingin.Adapter.ClimbingCategoryAdapter;
import com.example.vennydiski.myclimbingin.Config.AppConfig;
import com.example.vennydiski.myclimbingin.Config.AppController;
import com.example.vennydiski.myclimbingin.Model.ClimbingTool;
import com.example.vennydiski.myclimbingin.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mfarid on 23/06/18.
 */

public class ClimbingCategoryFragment extends Fragment {
    public List<ClimbingTool> climbingList;
    public ClimbingCategoryAdapter adapterClimbingCategory;
    Context context;

    public static ClimbingCategoryFragment newInstance() {
        ClimbingCategoryFragment fragment = new ClimbingCategoryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_climbing_category, container, false);
        Context context = this.getActivity().getApplicationContext();
        Bundle bundle = this.getArguments();
        Integer id = bundle.getInt("id");
        //Toast.makeText(context, Integer.toString(id), Toast.LENGTH_LONG).show();
        RecyclerView rv = (RecyclerView)(rootView.findViewById(R.id.rv_climbing_category));
        rv.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rv.setLayoutManager(linearLayoutManager);
        climbingList = new ArrayList<>();
        adapterClimbingCategory = new ClimbingCategoryAdapter(climbingList, context);
        rv.setAdapter(adapterClimbingCategory);
        getClimbingCategories(id);
        return rootView;
    }


    protected void getClimbingCategories(int categoryId)
    {
        //Log.d("iki get category", Integer.toString(categoryId));
        final StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                AppConfig.URL_ClIMBING_TOOL_BY_CATEGORY+categoryId,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("code") == 302) {
                                JSONArray jsonArrayFish = obj.getJSONArray("data");
                                refreshData(jsonArrayFish);
                            }
                            else {
                                Toast.makeText(getActivity().getApplicationContext(), "message", Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }

                    }
                },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity().getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            //TODO NAMPILKAN HASMAP DARI VARIABBLE NULL DARI GET DATA WEB
        };
        AppController.getInstance(getActivity().getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void refreshData(JSONArray recomendations) throws JSONException {
        climbingList.clear();
        for (int i = 0; i < recomendations.length(); i++) {
            JSONObject obj = recomendations.getJSONObject(i);
            Log.d("JSON", "valuenya :"+obj);
            climbingList.add(new ClimbingTool(
                    obj.getInt("id"),
                    obj.getString("climbing_tool_name"),
                    obj.getString("climbing_tool_category_name"),
                    obj.getString("climbing_tool_price"),
                    obj.getString("climbing_tool_stock"),
                    obj.getString("climbing_tool_picture"),
                    obj.getString("climbing_tool_categories_id")
            ));
        }

        adapterClimbingCategory.notifyDataSetChanged();
    }
}
