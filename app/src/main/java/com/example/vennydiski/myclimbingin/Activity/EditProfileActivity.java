package com.example.vennydiski.myclimbingin.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.vennydiski.myclimbingin.Config.AppConfig;
import com.example.vennydiski.myclimbingin.Config.AppController;
import com.example.vennydiski.myclimbingin.Config.SessionManager;
import com.example.vennydiski.myclimbingin.Config.VolleyMultipartRequest;
import com.example.vennydiski.myclimbingin.Model.DataPart;
import com.example.vennydiski.myclimbingin.Model.User;
import com.example.vennydiski.myclimbingin.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mfarid on 23/06/18.
 */

public class EditProfileActivity extends AppCompatActivity {
    public TextView textViewUsername, textViewUserIdentity;
    public EditText editTextFullName, editTextAddress, editTextConfirmPassword, editTextPostcode, editTextUserPassword, editTextUserPhoneNumber, editTextUserEmail;
    public Button submit, uploadImage;
    ProgressBar progressBar;
    Bitmap bitmap;
    SessionManager sessionManager;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.edit_profile_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        //Inisialisasi nilai yang didapat dari shared preferences
        preferences = getApplicationContext().getSharedPreferences("UserPref", 0);
        int userId = preferences.getInt("id", 0);
        String name = preferences.getString("name", "");
        String email = preferences.getString("email", "");
        String username = preferences.getString("username", "");
        String address = preferences.getString("address", "");
        String phoneNumber = preferences.getString("phone_number", "");
        String identityNumber = preferences.getString("identity_number", "");
        String postcode = preferences.getString("postcode", "");

        sessionManager = new SessionManager(getApplicationContext());

        //Tembak view
        /*textViewUsername = (TextView)findViewById(R.id.edit_username);
        textViewUserIdentity = (TextView)findViewById(R.id.edit_profile_identity_number);*/
        editTextFullName = (EditText)findViewById(R.id.edit_profile_full_name);
        editTextAddress = (EditText)findViewById(R.id.edit_profile_address);
        editTextConfirmPassword = (EditText)findViewById(R.id.edit_profile_confirmation_password);
        editTextUserPassword = (EditText)findViewById(R.id.edit_profile_password);
        editTextPostcode = (EditText)findViewById(R.id.edit_profile_postcode);
        editTextUserPhoneNumber = (EditText)findViewById(R.id.edit_profile_phone_number);
        editTextUserEmail = (EditText)findViewById(R.id.edit_profile_email);
        submit = (Button)findViewById(R.id.edit_profile_submit);
        uploadImage = (Button)findViewById(R.id.but);
        progressBar = (ProgressBar)findViewById(R.id.progress_edit_profile);

        /*textViewUsername.setText(username);
        textViewUserIdentity.setText(identityNumber);*/
        editTextFullName.setText(name);
        editTextAddress.setText(address);
        editTextPostcode.setText(postcode);
        editTextUserPhoneNumber.setText(phoneNumber);
        editTextUserEmail.setText(email);

        uploadImage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getApplicationContext(), "Proses edit", Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.VISIBLE);
                doEditProfile();
            }
        });

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {

            //getting the image Uri
            Uri imageUri = data.getData();
            try {
                //getting bitmap object from uri
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);

                //displaying selected image to imageview
                ImageView imageView = (ImageView)findViewById(R.id.edit_profile_preview);
                imageView.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private void doEditProfile()
    {
        String fullName, address, password, confirmPassword, postcode, phoneNumber, email;
        fullName = editTextFullName.getText().toString().trim();
        address = editTextAddress.getText().toString().trim();
        password = editTextUserPassword.getText().toString().trim();
        confirmPassword = editTextConfirmPassword.getText().toString().trim();
        postcode = editTextPostcode.getText().toString().trim();
        phoneNumber = editTextUserPhoneNumber.getText().toString().trim();
        email = editTextUserEmail.getText().toString().trim();

        if (TextUtils.isEmpty(fullName)) {
            editTextFullName.setError("Please enter full name");
            editTextFullName.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(address)) {
            editTextAddress.setError("Please enter address");
            editTextAddress.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            editTextUserPassword.setError("Please enter password");
            editTextUserPassword.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(confirmPassword)) {
            editTextConfirmPassword.setError("Please enter confirm password");
            editTextConfirmPassword.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(postcode)) {
            editTextPostcode.setError("Please enter postcode");
            editTextPostcode.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(phoneNumber)) {
            editTextUserPhoneNumber.setError("Please enter phone number");
            editTextUserPhoneNumber.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(email)) {
            editTextUserEmail.setError("Please enter email");
            editTextUserEmail.requestFocus();
            return;
        }

        progressBar.setVisibility(View.GONE);
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, AppConfig.URL_UPDATE_PROFILE,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();

                            JSONObject userJson = obj.getJSONObject("data");
                            User user = new User(
                                    userJson.getInt("id"),
                                    userJson.getString("username"),
                                    userJson.getString("fullname"),
                                    userJson.getString("identity_number"),
                                    userJson.getString("address"),
                                    userJson.getString("phone_number"),
                                    userJson.getString("email"),
                                    userJson.getString("post_code"),
                                    userJson.getString("picture")
                            );

                            // TODO ambil data ke page selanjutnya
                            sessionManager.createLoginSession(
                                    user.getFullname(),
                                    user.getEmail(),
                                    user.getUsername(),
                                    user.getIdentity_number(),
                                    user.getAddress(),
                                    user.getPostcode(),
                                    user.getPhone_number(),
                                    user.getId(),
                                    user.getPicture()
                            );

                            Intent i = new Intent(EditProfileActivity.this, MainActivity.class);
                            startActivity(i);
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
            * If you want to add more parameters with the image
            * you can do it here
            * here we have only one parameter with the image
            * which is tags
            * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                String fullName, address, password, confirmPassword, postcode, phoneNumber, email;
                fullName = editTextFullName.getText().toString().trim();
                address = editTextAddress.getText().toString().trim();
                password = editTextUserPassword.getText().toString().trim();
                confirmPassword = editTextConfirmPassword.getText().toString().trim();
                postcode = editTextPostcode.getText().toString().trim();
                phoneNumber = editTextUserPhoneNumber.getText().toString().trim();
                email = editTextUserEmail.getText().toString().trim();
                int userId = preferences.getInt("id", 0);


                Map<String, String> params = new HashMap<>();
                params.put("address", address);
                params.put("password", password);
                params.put("confirm_password", confirmPassword);
                params.put("post_code", postcode);
                params.put("phone_number", phoneNumber);
                params.put("name", fullName);
                params.put("email", email);
                params.put("id", Integer.toString(userId));

                Log.d("params", params.toString());

                return params;
            }

            /*
            * Here we are passing image by renaming it with a unique name
            * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("user_picture", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };

        //adding the request to volley
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance(getApplicationContext()).addToRequestQueue(volleyMultipartRequest);

    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
