package com.example.vennydiski.myclimbingin.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.vennydiski.myclimbingin.Config.AppConfig;
import com.example.vennydiski.myclimbingin.Config.AppController;
import com.example.vennydiski.myclimbingin.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Venny Diski on 03/06/2018.
 */

public class RegisterActivity extends AppCompatActivity{
    EditText inputFullname,inputUsername,inputPassword,inputKonfirmasi,inputEmail,inputAddress;
    Button btnregister;
    String fullname,username,password,konfirmasi,email,address;
    Intent startLogin;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        btnregister = (Button)findViewById(R.id.signInButton);
        startLogin = new Intent(RegisterActivity.this,LoginActivity.class);

        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetAllEditTextFromLayout();
               SetfullnameValueDariEditTextFullname();
               SetaddressValueDariEditTextAddress();
               SetEmailValueDariEditTextinputEmail();
               SetpasswordValueDariEditTextinputPassword();
               SetusernameValueDariEditTextinputUsername();
               SetkonfirmasiPasswordValueDariEditTextKonfimasi();

                if (fullname.isEmpty() && username.isEmpty() && address.isEmpty() && email.isEmpty() && password.isEmpty()){
                    Toast.makeText(RegisterActivity.this, "Semua Data Harus di Isi", Toast.LENGTH_SHORT).show();
                }else {
                    if(!password.equals(konfirmasi)){
                        Toast.makeText(RegisterActivity.this, "Password dan konfirmasi password tidak sama", Toast.LENGTH_SHORT).show();

                    }else{
                        doRegister();
                        startActivity(startLogin);

                    }
                }
            }
        });
    }
    private void SetfullnameValueDariEditTextFullname(){
        fullname = inputFullname.getText().toString();
    }

    private void SetkonfirmasiPasswordValueDariEditTextKonfimasi() {
        konfirmasi = inputKonfirmasi.getText().toString();
    }

    private void SetaddressValueDariEditTextAddress() {
        address = inputAddress.getText().toString();
    }

    private void SetpasswordValueDariEditTextinputPassword() {
        password = inputPassword.getText().toString();
    }

    private void SetusernameValueDariEditTextinputUsername() {
        username = inputUsername.getText().toString();
    }

    private void SetEmailValueDariEditTextinputEmail() {
        email = inputEmail.getText().toString();
    }

    private void GetAllEditTextFromLayout() {
        inputFullname = (EditText) findViewById(R.id.edtTxt_Register_fullname);
        inputUsername = (EditText) findViewById(R.id.edtTxt_Register_username);
        inputPassword = (EditText) findViewById(R.id.edtTxt_Register_password);
        inputKonfirmasi = (EditText) findViewById(R.id.edtTxt_Register_confirmpassword);
        inputEmail = (EditText) findViewById(R.id.edtTxt_Register_email);
        inputAddress = (EditText) findViewById(R.id.edtTxt_Register_address);
    }
    private void doRegister() {
        GetAllEditTextFromLayout();
        SetfullnameValueDariEditTextFullname();
        SetaddressValueDariEditTextAddress();
        SetEmailValueDariEditTextinputEmail();
        SetpasswordValueDariEditTextinputPassword();
        SetusernameValueDariEditTextinputUsername();
        SetkonfirmasiPasswordValueDariEditTextKonfimasi();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, AppConfig.URL_REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("code") == 302) {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username",username);
                params.put("fullname",fullname);
                params.put("password", password);
                params.put("address", address);
                params.put("email",email);
                return params;
            }
        };
        AppController.getInstance(this).addToRequestQueue(stringRequest);


    }

}
