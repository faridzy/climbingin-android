package com.example.vennydiski.myclimbingin.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.vennydiski.myclimbingin.Adapter.ClimbingCategoryAdapter;
import com.example.vennydiski.myclimbingin.Config.AppConfig;
import com.example.vennydiski.myclimbingin.Config.AppController;
import com.example.vennydiski.myclimbingin.Model.ClimbingTool;
import com.example.vennydiski.myclimbingin.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mfarid on 23/06/18.
 */

public class SearchActivity extends AppCompatActivity {

    SearchView searchView;
    public List<ClimbingTool> climbingList;
    public ClimbingCategoryAdapter adapterClimbingCategory;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        progressBar = (ProgressBar)findViewById(R.id.progressbar_search);
        searchView = (SearchView) findViewById(R.id.searchbar_climbing);
        searchView.setQuery("", false);
        search(searchView);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.search_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        RecyclerView rv = (RecyclerView)(findViewById(R.id.rv_searching_climbing));
        rv.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(linearLayoutManager);
        climbingList = new ArrayList<>();
        adapterClimbingCategory = new ClimbingCategoryAdapter(climbingList, getApplicationContext());
        rv.setAdapter(adapterClimbingCategory);


    }

    public void search(SearchView searchView)
    {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d("Search", "masuk"+newText);
                if(newText.length() > 0){
                    progressBar.setVisibility(View.VISIBLE);
                    getData(newText);
                }
                return true;
            }
        });
    }

    public void getData(final String query)
    {
        final StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                AppConfig.URL_SEARCH,
                new Response.Listener<String>(){
                    @Override
                    public void onResponse(String response) {
                        try{
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("code") == 302) {
                                progressBar.setVisibility(View.GONE);
                                //Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                                try {
                                    Thread.sleep(200);
                                    JSONArray jsonArrayFish = obj.getJSONArray("data");
                                    refreshData(jsonArrayFish);

                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                            else {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        }catch (JSONException ex){
                            ex.printStackTrace();
                        }

                    }
                },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.d("error koneksi", error.getMessage());
                //progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Gagal Koneksi", Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("query", query);
                return params;
            }
        };
        AppController.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }

    private void refreshData(JSONArray recomendations) throws JSONException {
        climbingList.clear();
        for (int i = 0; i < recomendations.length(); i++) {
            JSONObject obj = recomendations.getJSONObject(i);
            Log.d("JSON", "valuenya :"+obj);
            climbingList.add(new ClimbingTool(
                    obj.getInt("id"),
                    obj.getString("climbing_tool_name"),
                    obj.getString("climbing_tool_category_name"),
                    obj.getString("climbing_tool_price"),
                    obj.getString("climbing_tool_stock"),
                    obj.getString("climbing_tool_picture"),
                    obj.getString("climbing_tool_categories_id")
            ));
        }

        adapterClimbingCategory.notifyDataSetChanged();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
