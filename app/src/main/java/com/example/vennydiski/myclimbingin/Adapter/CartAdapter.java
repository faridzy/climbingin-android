package com.example.vennydiski.myclimbingin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.vennydiski.myclimbingin.Config.AppConfig;
import com.example.vennydiski.myclimbingin.Config.AppController;
import com.example.vennydiski.myclimbingin.Model.OrderItem;
import com.example.vennydiski.myclimbingin.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mfarid on 04/06/18.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {
    private List<OrderItem> listCart;
    private Context mContext;
    public static int total = 0;
    public CartAdapter(List<OrderItem> recomendations, Context context){
        this.listCart = recomendations;
        this.mContext = context;
    }

    @Override
    public int getItemCount() {
        return listCart.size();
    }

    @Override
    public CartViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        this.mContext = viewGroup.getContext();
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cart_view_holder, viewGroup, false);
        CartViewHolder hvh = new CartViewHolder(v);
        return hvh;
    }

    @Override
    public void onBindViewHolder(CartViewHolder cartViewHolder, int i) {
        cartViewHolder.cart =listCart.get(i);
        Log.d("Adapter", "item id : " + listCart.get(i).getQuantity());
        cartViewHolder.context = this.mContext;
        cartViewHolder.climbingName.setText(listCart.get(i).getClimbing_tool_name());
        cartViewHolder.climbingQuantity.setText(listCart.get(i).getQuantity());
        int price = Integer.valueOf(listCart.get(i).getPrice());
        float tmpPrice = Float.valueOf(price);
        price = Math.round(tmpPrice);
        cartViewHolder.climbingPrice.setText("Rp. "+AppConfig.formatDecimal(price));


        String imgUrl = listCart.get(i).getPicture();
        Glide.with(mContext).load(AppConfig.ROOT_URL + imgUrl)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(cartViewHolder.climbingPicture);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class CartViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView climbingName;
        TextView climbingQuantity;
        TextView climbingPrice;
        ImageView climbingPicture;
        Button addQuantity, reduceQuantity;
        ProgressBar progressBar;
        int userId;

        public OrderItem cart;
        public Context context;

        CartViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv_cart);
            climbingName = (TextView)itemView.findViewById(R.id.cart_climbing_name);
            climbingQuantity = (TextView)itemView.findViewById(R.id.cart_climbing_quantity);
            climbingPrice = (TextView)itemView.findViewById(R.id.cart_climbing_price);
            climbingPicture = (ImageView)itemView.findViewById(R.id.cart_climbing_picture);
            progressBar = (ProgressBar)itemView.findViewById(R.id.progress_bar_add_quantity);
            addQuantity = (Button) itemView.findViewById(R.id.add_quantity);
            reduceQuantity = (Button) itemView.findViewById(R.id.reduce_quantity);

            addQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressBar.setVisibility(View.VISIBLE);
                    SharedPreferences preferences = context.getSharedPreferences("UserPref", 0);
                    userId =  preferences.getInt("id", 0);

                    addOrderQuantity();
                }
            });

            reduceQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressBar.setVisibility(View.VISIBLE);
                    SharedPreferences preferences = context.getSharedPreferences("UserPref", 0);
                    userId =  preferences.getInt("id", 0);

                    reduceOrderQuantity();
                }
            });

        }


        private void refresh()
        {
            Intent intent = ((AppCompatActivity) context).getIntent();
            ((AppCompatActivity) context).overridePendingTransition(0, 0);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            ((AppCompatActivity) context).finish();
            ((AppCompatActivity) context).overridePendingTransition(0, 0);
            ((AppCompatActivity) context).startActivity(intent);
        }


        private void addOrderQuantity()
        {
            Log.d("url", AppConfig.URL_ADD_QUANTITY);
            final StringRequest stringRequest = new StringRequest(
                    Request.Method.POST,
                    AppConfig.URL_ADD_QUANTITY,
                    new Response.Listener<String>(){
                        @Override
                        public void onResponse(String response) {
                            try{
                                JSONObject obj = new JSONObject(response);
                                if (obj.getInt("code") == 302) {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(context, obj.getString("message"), Toast.LENGTH_SHORT).show();
                                    refresh();
                                }
                                else {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(context, obj.getString("message"), Toast.LENGTH_SHORT).show();
                                }

                            }catch (JSONException ex){
                                ex.printStackTrace();
                            }

                        }
                    },new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(context, "Gagal Koneksi", Toast.LENGTH_SHORT).show();
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", Integer.toString(userId));
                    params.put("order_item_id", Integer.toString(cart.getOrder_item_id()));
                    return params;
                }
            };
            AppController.getInstance(context).addToRequestQueue(stringRequest);
        }


        private void reduceOrderQuantity()
        {
            Log.d("url", AppConfig.URL_REDUCE_QUANTITY);
            final StringRequest stringRequest = new StringRequest(
                    Request.Method.POST,
                    AppConfig.URL_REDUCE_QUANTITY,
                    new Response.Listener<String>(){
                        @Override
                        public void onResponse(String response) {
                            try{
                                JSONObject obj = new JSONObject(response);
                                if (obj.getInt("code") == 302) {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(context, obj.getString("message"), Toast.LENGTH_SHORT).show();
                                    refresh();
                                }
                                else {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(context, obj.getString("message"), Toast.LENGTH_SHORT).show();
                                }

                            }catch (JSONException ex){
                                ex.printStackTrace();
                            }

                        }
                    },new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(context, "Gagal Koneksi", Toast.LENGTH_SHORT).show();
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", Integer.toString(userId));
                    params.put("order_item_id", Integer.toString(cart.getOrder_item_id()));
                    return params;
                }
            };
            AppController.getInstance(context).addToRequestQueue(stringRequest);
        }

    }

}
