package com.example.vennydiski.myclimbingin.Config;

import java.text.DecimalFormat;

/**
 * Created by Venny Diski on 03/06/2018.
 */

public class AppConfig {
    public static final String ROOT_URL = "http://222.124.168.221/climbing/";
    public static final String URL_REGISTER = ROOT_URL + "api/register";
    public static final String URL_LOGIN = ROOT_URL + "api/login";
    public static final String URL_ClIMBING_TOOL_BY_CATEGORY = ROOT_URL + "api/climbing-tool/filter?category=";
    public static final String URL_ClIMBING_TOOL_BY_ID = ROOT_URL + "api/climbing-tool/filter?climbing-tool=";
    public static final String URL_ADD_TO_CART= ROOT_URL + "api/cart/add-to-cart";
    public static final String URL_LIST_CART = ROOT_URL + "api/cart/";
    public static final String URL_ADD_QUANTITY = ROOT_URL + "/api/cart/add-quantity";
    public static final String URL_REDUCE_QUANTITY = ROOT_URL + "/api/cart/reduce-quantity";
    public static final String URL_RECOMENDATION = ROOT_URL + "api/climbing-tool/recommendation";
    public static final String URL_HISTORY = ROOT_URL + "api/history";
    public static final String URL_CHECKOUT = ROOT_URL + "api/payment/checkout";
    public static final String URL_UPLOAD_RECEIPT = ROOT_URL + "api/payment/upload-receipt";
    public static final String URL_UPDATE_PROFILE = ROOT_URL+ "api/update-profile";
    public static final String URL_STORE_FEEDBACK = ROOT_URL+ "api/feedback/save";
    public static final String URL_SEARCH = ROOT_URL+ "api/search";
    public static final String URL_GET_PROVINCE = ROOT_URL+ "api/raja-ongkir/province";
    public static final String URL_GET_CITY = ROOT_URL+ "api/raja-ongkir/city";
    public static final String URL_GET_COST = ROOT_URL+ "api/raja-ongkir/cost";

    public static String formatDecimal(int number) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        return formatter.format(number);
    }
}
