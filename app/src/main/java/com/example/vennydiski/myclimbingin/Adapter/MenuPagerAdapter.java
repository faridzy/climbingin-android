package com.example.vennydiski.myclimbingin.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.vennydiski.myclimbingin.Fragment.HelpFragment;
import com.example.vennydiski.myclimbingin.Fragment.HistoryFragment;
import com.example.vennydiski.myclimbingin.Fragment.HomeFragment;
import com.example.vennydiski.myclimbingin.Fragment.ProfileFragment;

/**
 * Created by mfarid on 23/06/18.
 */

public class MenuPagerAdapter extends FragmentStatePagerAdapter {
    int numberOfTab;


    public MenuPagerAdapter(FragmentManager fm, int mNumbOfTab) {
        super(fm);
        this.numberOfTab = mNumbOfTab;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0 :
                return new HomeFragment();
            case 1 :
                return new HistoryFragment();
            case 2:
                return new HelpFragment();
            case 3:
                return new ProfileFragment();
            default:
                return new HomeFragment();
        }
    }

    @Override
    public int getCount() {
        return numberOfTab;
    }

}
