package com.example.vennydiski.myclimbingin.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.vennydiski.myclimbingin.Activity.UploadBayarActivity;
import com.example.vennydiski.myclimbingin.Config.AppConfig;
import com.example.vennydiski.myclimbingin.Config.AppController;
import com.example.vennydiski.myclimbingin.Model.History;
import com.example.vennydiski.myclimbingin.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Created by mfarid on 23/06/18.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {

    List<History> historyList;
    private Context mContext;
    public HistoryAdapter(List<History> recomendations, Context context){
        this.historyList = recomendations;
        this.mContext = context;
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        this.mContext = viewGroup.getContext();
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.history_view_holder, viewGroup, false);
        HistoryViewHolder hvh = new HistoryViewHolder(v);
        return hvh;
    }

    @Override
    public void onBindViewHolder(HistoryViewHolder historyViewHolder, int i) {
        Log.d("Adapter", "item id : " + i);
        historyViewHolder.history = historyList.get(i);
        historyViewHolder.context = this.mContext;
        historyViewHolder.orderName.setText(historyList.get(i).getOrderName());
        historyViewHolder.orderAddress.setText(historyList.get(i).getOrderAddress());
        historyViewHolder.orderDate.setText(historyList.get(i).getOrderDate());
        historyViewHolder.orderStatus.setText(historyList.get(i).getOrderStatus());
        int total = Integer.valueOf(historyList.get(i).getOrderTotal());
        historyViewHolder.orderTotal.setText("Rp. "+ AppConfig.formatDecimal(total));

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class HistoryViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView orderDate, orderTotal, orderStatus, orderName, orderAddress;
        public History history;
        public Context context;
        public int userOrderId, feedbackValue;

        HistoryViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv_item_history);
            orderDate = (TextView)itemView.findViewById(R.id.history_order_date);
            orderTotal = (TextView)itemView.findViewById(R.id.history_order_total);
            orderStatus = (TextView)itemView.findViewById(R.id.history_order_status);
            orderName = (TextView)itemView.findViewById(R.id.history_order_name);
            orderAddress = (TextView)itemView.findViewById(R.id.history_order_address);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(history.getOrderFeedbackStatus() == 0){
                        if(history.getOrderIsPay().equalsIgnoreCase("0")){
                            //Belum bayar
                            Bundle bundle = new Bundle();
                            bundle.putString("order_id", history.getOrderId());
                            bundle.putString("order_total", history.getOrderTotal());

                            Intent i = new Intent(((AppCompatActivity) context), UploadBayarActivity.class);
                            i.putExtras(bundle);
                            context.startActivity(i);


                        }else{
                            openDialog(history.getOrderId());
                        }
                    }else{
                        Toast.makeText(context, "Sudah isi feedback", Toast.LENGTH_LONG).show();
                    }

                }
            });

        }




        public void openDialog(final String orderId) {
            final Dialog dialog = new Dialog(context);
            dialog.setContentView(R.layout.dialog_feedback);
            dialog.setTitle("Feedback Kepuasan Layanan");
            dialog.show();

            Button dialogOk, dialogCancel;
            dialogOk = dialog.findViewById(R.id.dialog_ok);
            dialogCancel = dialog.findViewById(R.id.dialog_cancel);

            dialogOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(context, "Anda memilih puas untuk order "+orderId, Toast.LENGTH_LONG).show();
                    try {
                        userOrderId = Integer.valueOf(orderId);
                        feedbackValue = 1;
                        storeFeedback();
                        Thread.sleep(30);
                        dialog.dismiss();
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            });

            dialogCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(context, "Anda memilih tidak puas untuk order "+orderId, Toast.LENGTH_LONG).show();
                    try {
                        userOrderId = Integer.valueOf(orderId);
                        feedbackValue = 0;
                        storeFeedback();
                        Thread.sleep(30);
                        dialog.dismiss();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });


        }

        private void refresh()
        {
            Intent intent = ((AppCompatActivity) context).getIntent();
            ((AppCompatActivity) context).overridePendingTransition(0, 0);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            ((AppCompatActivity) context).finish();
            ((AppCompatActivity) context).overridePendingTransition(0, 0);
            ((AppCompatActivity) context).startActivity(intent);
        }

        private void storeFeedback()
        {
            Log.d("url", AppConfig.URL_STORE_FEEDBACK);
            final StringRequest stringRequest = new StringRequest(
                    Request.Method.POST,
                    AppConfig.URL_STORE_FEEDBACK,
                    new Response.Listener<String>(){
                        @Override
                        public void onResponse(String response) {
                            try{
                                JSONObject obj = new JSONObject(response);
                                if (obj.getInt("code") == 302) {
                                    //progressBar.setVisibility(View.GONE);
                                    Toast.makeText(context, obj.getString("message"), Toast.LENGTH_SHORT).show();
                                    refresh();
                                }
                                else {
                                    //progressBar.setVisibility(View.GONE);
                                    Toast.makeText(context, obj.getString("message"), Toast.LENGTH_SHORT).show();
                                }

                            }catch (JSONException ex){
                                ex.printStackTrace();
                            }

                        }
                    },new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //progressBar.setVisibility(View.GONE);
                    Toast.makeText(context, "Gagal Koneksi", Toast.LENGTH_SHORT).show();
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("order_id", Integer.toString(userOrderId));
                    params.put("feedback_value", Integer.toString(feedbackValue));
                    return params;
                }
            };
            AppController.getInstance(context).addToRequestQueue(stringRequest);
        }

    }


}
