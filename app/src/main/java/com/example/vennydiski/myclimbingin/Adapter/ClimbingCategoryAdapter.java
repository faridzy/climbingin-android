package com.example.vennydiski.myclimbingin.Adapter;

/**
 * Created by mfarid on 23/06/18.
 */
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.vennydiski.myclimbingin.Activity.DetailClimbingActivity;
import com.example.vennydiski.myclimbingin.Model.ClimbingTool;
import com.example.vennydiski.myclimbingin.Config.AppConfig;
import com.example.vennydiski.myclimbingin.R;


import java.util.List;

public class ClimbingCategoryAdapter extends RecyclerView.Adapter<ClimbingCategoryAdapter.ClimbingCategoryViewHolder> {

    List<ClimbingTool> climbingList;
    private Context mContext;
    public ClimbingCategoryAdapter(List<ClimbingTool> recomendations, Context context){
        this.climbingList = recomendations;
        this.mContext = context;
    }

    @Override
    public int getItemCount() {
        return climbingList.size();
    }

    @Override
    public ClimbingCategoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        this.mContext = viewGroup.getContext();
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.climbing_category_view_holder, viewGroup, false);
        ClimbingCategoryViewHolder hvh = new ClimbingCategoryViewHolder(v);
        return hvh;
    }

    @Override
    public void onBindViewHolder(ClimbingCategoryViewHolder climbingCategoryViewHolder, int i) {
        Log.d("Adapter", "item id : " + i);
        climbingCategoryViewHolder.climbing=climbingList.get(i);
        climbingCategoryViewHolder.context=this.mContext;
        climbingCategoryViewHolder.climbingName.setText(climbingList.get(i).getName());
        climbingCategoryViewHolder.climbingPrice.setText("Rp. "+AppConfig.formatDecimal(Integer.valueOf(climbingList.get(i).getPrice())));
        climbingCategoryViewHolder.description.setText("Sisa stok : "+climbingList.get(i).getStock());

        String imgUrl = climbingList.get(i).getPicture();
        Glide.with(mContext).load(AppConfig.ROOT_URL + imgUrl)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(climbingCategoryViewHolder.climbingPicture);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ClimbingCategoryViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView climbingName;
        TextView climbingPrice;
        ImageView climbingPicture;
        TextView description;

        public ClimbingTool climbing;
        public Context context;

        ClimbingCategoryViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv_climbing_category);
            climbingName = (TextView)itemView.findViewById(R.id.fc_climbing_name);
            //fishCategory = (TextView)itemView.findViewById(R.id.fc_fish_category);
            climbingPrice = (TextView)itemView.findViewById(R.id.fc_price);
            climbingPicture = (ImageView)itemView.findViewById(R.id.fc_climbing_picture);
            description = (TextView)itemView.findViewById(R.id.fc_climbing_description);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("id",climbing.getId());
                    bundle.putString("climbing_tool_name", climbing.getName());
                    bundle.putString("climbing_category_name", climbing.getCategory_name());
                    bundle.putString("climbing_tool_price", climbing.getPrice());
                    bundle.putString("climbing_tool_stock",climbing.getStock());
                    bundle.putString("climbing_tool_picture", climbing.getPicture());

                    Intent i = new Intent(((AppCompatActivity) context), DetailClimbingActivity.class);
                    i.putExtras(bundle);
                    context.startActivity(i);

                }
            });

        }
    }

}
