package com.example.vennydiski.myclimbingin.Model;

/**
 * Created by mfarid on 24/06/18.
 */

public class Cost {
    String service, description, cost, etd;

    public Cost(){}

    public Cost(String service, String description, String cost, String etd)
    {
        this.service = service;
        this.description = description;
        this.cost = cost;
        this.etd = etd;
    }

    public void setService(String service)
    {
        this.service = service;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setCost(String cost)
    {
        this.cost = cost;
    }

    public void setEtd(String etd)
    {
        this.etd = etd;
    }

    public String getService()
    {
        return this.service;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getCost()
    {
        return this.cost;
    }

    public String getEtd()
    {
        return this.etd;
    }
}
