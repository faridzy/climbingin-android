package com.example.vennydiski.myclimbingin.Model;

/**
 * Created by Venny Diski on 03/06/2018.
 */

public class User {
    int id;
    String fullname,username,password,identity_number,address,phone_number,email,postcode, picture;

    public User(int id, String username, String name, String identity_number, String address, String phone_number, String email, String post_code, String picture) {
        this.id = id;
        this.fullname = fullname;
        this.username = username;
        this.identity_number = identity_number;
        this.address = address;
        this.phone_number = phone_number;
        this.email = email;
        this.postcode = postcode;
        this.picture = picture;
    }

    public String getFullname() { return fullname; }

    public void setFullname(String fullname) { this.fullname = fullname; }

    public void setPicture(String picture)
    {
        this.picture = picture;
    }

    public String getPicture()
    {
        return this.picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIdentity_number() {
        return identity_number;
    }

    public void setIdentity_number(String identity_number) {
        this.identity_number = identity_number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }
}
